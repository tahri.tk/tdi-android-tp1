package tk.tahri.tp1;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

     Button buttonCalculer;
     Button buttonEffacer;
     TextView textViewResultat;
     EditText editTextN1;
     EditText editTextN2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonCalculer=findViewById(R.id.buttonCalculer);
        buttonEffacer=findViewById(R.id.buttonEffacer);
        textViewResultat=findViewById(R.id.textViewResultat);
        editTextN1=findViewById(R.id.editTextN1);
        editTextN2=findViewById(R.id.editTextN2);


        buttonCalculer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!editTextN1.getText().toString().isEmpty() && !editTextN2.getText().toString().isEmpty()){
                    int n1= Integer.parseInt(editTextN1.getText().toString());
                    int n2= Integer.parseInt(editTextN2.getText().toString());
                    int r= n1+n2;
                    textViewResultat.setText(String.valueOf(r));
                }
            }
        });

        buttonEffacer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editTextN1.setText("");
                editTextN2.setText("");
                textViewResultat.setText("");
                Toast.makeText(getApplicationContext(), "Reste", Toast.LENGTH_SHORT).show();

            }
        });

    }
}
